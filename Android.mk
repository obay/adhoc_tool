LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := adhoc_toolbox
LIB_PATH := $(LOCAL_PATH)
LOCAL_SHARED_LIBRARIES := wpa_client
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)
LOCAL_SRC_FILES := main.cpp network.cpp
LOCAL_CPPFLAGS := -std=gnu++11 -Wall -fPIC -pthread         # whatever g++ flags you like
LOCAL_LDLIBS := -L$(SYSROOT)/system/bin -llog -pie -pthread   # whatever ld flags you like
include $(BUILD_EXECUTABLE)    # <-- Use this to build an executable.

include $(CLEAR_VARS)
LOCAL_MODULE := wpa_client
LOCAL_SRC_FILES := $(TARGET_ARCH_ABI)/libwpa_client.so
include $(PREBUILT_SHARED_LIBRARY)
