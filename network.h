#ifndef NETWORK_H
#define NETWORK_H

#include <string>
#include <cstdio>
#include "wpa_ctrl.h"
#include <exception>
#include <errno.h>
#include <string>
#include <string.h>
#include <stdexcept>
#include <memory>
#include <sstream>
#include <iostream>
#include <vector>
#include <unistd.h>
using namespace std;

template <typename T>
/**
 * @brief Replacement for function std::to_string which is missing in android.
 *
 * @param val the value to be converted to string.
 * @return std::string The resulted string.
 */
std::string tostring(T val)
{
    std::ostringstream out_stream;
    out_stream << val ;
    return out_stream.str() ;
}

/**
 * @brief Will convert str to integer.
 *
 * @param str the string to be converted (only positive integers are supported).
 * @return int the resulting integer.
 */
int custom_stoi(const std::string& str);
/**
 * @brief Replacement for function std::isdigit which is missing in android.
 *
 * @param ch the character to be tested.
 * @return bool returns true if ch is a number (0-9).
 */
bool custom_isdigit(char& ch);
/**
 * @brief Converts cmd to upper case string.
 *
 * @param cmd the string to be converted.
 */
void toupper(string& cmd);
/**
 * @brief Print a message to standard output stream.
 *
 * @param message the messagem to be printed
 */
void debug(const string& message);

/**
 * @brief Indicates that setting a network variable (like ssid) had failed.
 *
 */
class Variable_error_exception: public exception{
    string message; /**< The error message that should be displayed. */
public:
    /**
     * @brief The constructor will set the message of the exception.
     *
     * @param name the name of the variable causing the error.
     * @param value the assigned value.
     */
    Variable_error_exception(string name, string value);
    /**
     * @brief Overrides exception::what to return the appropriate message.
     *
     * @return const char reutrns a C string representation from member variable message.
     */
    virtual const char* what() const throw();
};

/**
 * @brief
 *
 */
class Errno_exception : public exception{
    string message; /**< TODO */
public:
    /**
     * @brief
     *
     * @param message
     */
    Errno_exception(string message);
    /**
     * @brief
     *
     * @return const char
     */
    virtual const char* what() const throw();
};


class WifiManager;

/**
 * @brief Network class holds network information.
 *
 */
class Network{
friend class WifiManager;
public:
    /**
     * @brief The constructor requires the SSID, and will initialize other variables to their default values.
     *
     * @param ssid Service Set Identifier is the unique name for the wireless LAN.
     */
    Network(string ssid);

    /**
     * @brief print network's id and ssid to the standard output.
     *
     */
    void print() const;
    /**
     * @brief print all the network variables values to the standard output.
     *
     */
    void print_verbose() const;

private:

public:
    string ssid; /**< Service Set Identifier or SSID is the unique name of the wireless LAN that is visible to the user. */
    int priority; /**< Networks with larger priority will be prefered to be associated with. */
    int frequency; /**< Radio frequency of the network. */
    int mode;/**< Possible values are 0: infrastructure mode, 1: ibss(point-to-point), 2: access point (AP). */
    string key_mgmt; /**< Key management protocol (for authentication) */
    int scan_ssid; /**< If set to 1 wpa_supplicant will scan this network with its SSID. */

    string bssid; /**< If it is set wpa_supplicant will only associate with access points that using the same bssid.
 It is usualy set to the MAC address of the access point.*/
    string flags; /**< Indicates the current state of the network (e.g: enabled, disabled, or set as current). This
  variable is provided by wpa_supplicant when sending the command "LIST_NETWORKS".*/

private:
    int id; /**< The netowrk's identifier that is given by wpa_supplicant to identify the network locally. */
};
// ************************ wifi manager aka wpa_client
/**
 * @brief WifiManager is the class that handles the communcication with wpa_supplicant.
 *
 */
class WifiManager{

public:
    /**
     * @brief The constructor will open the connection wit wpa__supplicant using the specified socket.`
     *
     * @param socket the path to the communcication socket.
     */
    WifiManager(string socket);

    /**
     * @brief Will create a network if it does not already exists.
     *
     * @param network The network to be created.
     */
    void create_network(Network& network) const;
    /**
     * @brief Update the network's values it it already exists else through invalid_argument exception.
     *
     * @param network The network to be updated.
     */
    void update_network(Network& network) const;
    /**
     * @brief Make wpa_supplicant associate with this network.
     *
     * @param network The network to be associated with.
     * @param ap_scan This defines the strategy of access point scanning --see wpa_supplicant documentation.
     * @return bool returns true if the command "SET_NETWORK" succeed else false.
     */
    bool set_network_as_current(const Network& network, int ap_scan=-1) const;
    /**
     * @brief Will set the network's variable with the provided value.
     *
     * @param network The network to be configured.
     * @param variable The name of the variable.
     * @param value The value of the variable.
     */
    void set_variable(Network network, string variable, string value) const;
    /**
     * @brief Sends the command "LIST_NETWORKS" to wpa_supplicant and create Network objects based on the results.
     *
     * @param print_output Whether to print the commands reply to standard output.
     * @return vector<Network> Returns a vector with all the networks from the command's reply.
     */
    vector<Network> list_networks(bool print_output=false) const;
    /**
     * @brief Checks if a network with the same SSID already exists in the list of networks returned by WifiManager::list_networks().
     *
     * @param network The network to be searched for.
     * @return bool Returns true if SSID matches.
     */
    bool ssid_exists(Network& network) const;
    /**
     * @brief Checks if a network with the same BSSID already exists in the list of networks returned by WifiManager::list_networks().
     *
     * @param network The network to be searched for.
     * @return bool Returns true if BSSID matches.
     */
    bool bssid_exists(Network& network) const;
    /**
     * @brief Sends a command to wpa_supplicant by calling wpa_ctrl_request().
     *
     * @param cmd The command to be sent.
     * @param msg_cb A function to be called by wpa_supplicant for unsolicited communication.
     * @param print_command Whether to print the command's reply to standard output.
     * @return string Returns the reply from wpa_supplicant.
     */
    string send_command(const string cmd, void (*msg_cb)(char *msg, size_t len), bool print_command=true) const;

    /**
     * @brief Checks if the reply equals to "OK" which indicates success.
     *
     * @param reply The reply to be checked.
     * @return bool Returns true if the reply equals to "OK".
     */
    bool is_reply_ok(const string& reply) const;

    /**
     * @brief The destructor will close the connection with wpa_suppplicant and free any resource used by WifiManager object.
     *
     */
    ~WifiManager();
private:

    /**
     * @brief Sets all network variables by calling WifiManager::set_variable() for each variable.
     *
     * @param network The network to be operated on.
     */
    void set_all_network_variables(Network network) const;
    /**
     * @brief Opens the connection with wpa_supplicant by calling wpa_ctrl_open().
     *
     */
    void open_connection();

    string socket; /**< The path to the communication socket of the wpa_supplicant control interface. */
    wpa_ctrl* ctrl; /**< This variable holdes inforamtion about the interprocess communication with wpa_supplicant control interface */
    size_t* max_reply_len; /**< The maximum reply length to be recieved from wpa_supplicant when sending a command. */
    char* _reply; /**< Holds the last reply recieved from wpa_supplicant. */
};

#endif // NETWORK_H
