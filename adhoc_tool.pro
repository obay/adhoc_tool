TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    network.cpp

HEADERS += \
    wpa_ctrl.h \
    network.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../project/release/ -lwpa_client
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../project/debug/ -lwpa_client
else:unix: LIBS += -L$$PWD/../../../project/ -lwpa_client

INCLUDEPATH += $$PWD/../../../project
DEPENDPATH += $$PWD/../../../project

DISTFILES += \
    libwpa_client.so \
    adhoc_tool.pro.user \
    Android.mk \
    Application.mk \
    com_out.txt \
    default.properties
