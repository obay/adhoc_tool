#include <errno.h>
#include <cstdlib>
#include <exception>
#include <cstring>
#include "wpa_ctrl.h"
#include <iostream>
#include <sys/types.h>
#include <string>
#include <math.h>
#include "network.h"
//#include "javainterface.h"
using namespace std;

/**
 * @brief Use this function to send commands to wpa_supplicant.
 *
 * @param command The command to send.
 * @param socket The path to the interprocess communication socket of
 * wpa_supplicant control interface.
 */
void send_command(string command, string socket){
    WifiManager wifi(socket);
    wifi.send_command(command, NULL);
}

/**
 * @brief Create and associate with the "Public Adhoc Network" network or update
 *  it if it is already exists.
 *
 * @param socket The path to the interprocess communication socket of
 * wpa_supplicant control interface.
 * @return bool Returns true if all commands succeed else an exception is
 * thrown.
 */
bool connect_to_public_network(string socket){
    Network network(string("Public Adhoc Network"));
    network.mode = 1;
    network.frequency = 2457;
    network.key_mgmt = string("NONE");
    network.priority = 50;
    network.bssid = "b4:ef:39:17:48:0f";
//    network.scan_ssid = 1;
    WifiManager wifi(socket);
//    wifi.list_networks();
    if(wifi.ssid_exists(network)){
        cout << "ssid_exists returned true" << endl;
        wifi.update_network(network);
    }
    else{
        cout << "ssid_exists returned false" << endl;
        wifi.create_network(network);
    }

    bool res = wifi.set_network_as_current(network, 2);
    wifi.send_command("SAVE_CONFIG", NULL);
    return res;
}

/**
 * @brief Reverse the changes made by connect_to_public_network(). Namely set
 * "Public Adhoc Network" network's priority to
 *  its default value(1) and also set ap_scan to its default value (1).
 *
 * @param socket The path to the interprocess communication socket of
 * wpa_supplicant control interface.
 * @return bool Returns true if all commands succeed.
 */
bool return_ap_scan_and_priority_to_normal(string socket){
    WifiManager wifi(socket);
    //todo: review returning priority to normal.
    Network network(string("Public Adhoc Network"));
    if(wifi.ssid_exists(network))
        wifi.set_variable(network, "priority", "0");

    string reply = wifi.send_command("AP_SCAN 1", NULL);
    network.print_verbose();
    return wifi.is_reply_ok(reply);
  }

/**
 * @brief This program is run as a command from the terminal or by using Shell
 * class in Java. It must be run as root or at least as the user owning the IPC
 * (Interprocess Communication) socket. The application accepts three options:
 * -i [socket location] : is used to specify the path to the communication
 * socket, by default it is set to “/data/misc/wifi/sockets/wlan0”.
 * -c : create the “Public Adhoc Network” network and set its properties
 * (mode of operation, frequency, priority, security, and SSID) and configure
 * the wireless interface to connect to the network (set ap_scan to 2, to make
 * the driver scan for the network by its SSID and if not found create the
 * network, and then announce the network existence by sending beacons.
 * -r : reverse the commands that are executed when the -c option is given.
 * Namely set “Public Adhoc Network” priority to 1(the default value) and set
 *  ap_scan to 1(the default value).
 * -x [command] : will send the given command to wpa_supplicant. Available
 * commands can be found in wpa_supplicant documentations. For example the
 * command “LIST_NETWORKS” will reply with a list of all the networks found in
 * wpa_supplicant configuration file.
 */
int main(int argc, char** argv){
    int success = 1;
    try{
    int i=1;
    char* socket = new char[256];
    strcpy(socket, "/data/misc/wifi/sockets/wlan0");

    while(argc>1){
        if(!strcmp(argv[i], "-c")){
            if(connect_to_public_network(socket))
                success = 0;
            else
                success = 1;
        }
        if(!strcmp(argv[i], "-r")){
            if(return_ap_scan_and_priority_to_normal(socket))
                success = 0;
            else
                success = 1;
        }
        if(!strcmp(argv[i], "-x")){
            char* cmd = new char(50);
            strcpy(cmd, argv[i+1]);
            --argc;
            send_command(cmd, socket);
        }
        if(!strcmp(argv[i], "-i")){ 
            strcpy(socket, argv[i+1]);
            --argc;
            ++i;
        }
        ++i;
        --argc;
    }
    }catch(Variable_error_exception e){
        cout << "Exception: " << e.what() << endl;
        return -1;
    }catch(Errno_exception e){
        cout << "Exception: " << e.what() << endl;
        return -1;
    }catch(invalid_argument e){
        cout << "Exception: " << e.what() << endl;
        return -1;
    }catch(logic_error e){
        cout << "Exception: " << e.what() << endl;
        return -1;
    }catch(exception e){
        cout << "Exception: " << e.what() << endl;
        return -1;
    }
    return success;
}
